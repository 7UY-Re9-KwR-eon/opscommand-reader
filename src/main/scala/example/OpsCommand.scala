package example

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.Materializer
import akka.stream.scaladsl.{Keep, Sink}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}

object OpsCommand extends App {
  val system = ActorSystem()
  implicit val mat: Materializer = Materializer(system)

  Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        system.terminate()
      }
  })

  try {
    val config = system.settings.config.getConfig("akka.kafka.consumer")
    val consumerSettings =
      ConsumerSettings(config, new StringDeserializer, new ByteArrayDeserializer)
        .withGroupId("group1")
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")


    val source = Consumer.plainSource(consumerSettings, Subscriptions.topicPattern("fxt\\.opscommand\\..*"))
    val sink = Sink.foreach[ConsumerRecord[String, Array[Byte]]](r => println(r))

    val runnable = source.filter(_.key().startsWith("HOL001")).toMat(sink)(Keep.right)
    runnable.run()

  } catch {
    case _ @ e =>
      e.getStackTrace
      system.terminate()
  }
}
